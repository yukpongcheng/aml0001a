create table accrual_airport_pair
   (	fkx_o_airport_code varchar(4) not null ,
	fkx_d_airport_code varchar(4) not null ,
	marketing_mileage int(9),
	fkx1_carriercode varchar(3) not null ,
	flt_leg_type_code varchar(1),
	inactive_indicator varchar(1),
	modified_date datetime default current_timestamp not null ,
	 constraint pk_accrual_airport_pair primary key (fkx_o_airport_code, fkx_d_airport_code, fkx1_carriercode)

   );



    create table carrier
   (	carrier_code varchar(3) not null ,
	carrier_number int(3),
	airline_designator varchar(2),
	carrier_name varchar(80) not null ,
	last_updt_date datetime,
	last_updt_time datetime,
	purge_date datetime,
	fk1_carrier_code varchar(3),
	disallow_open_jaw varchar(1) default 'n' not null ,
	disallow_stopover varchar(1) default 'n' not null ,
	disallow_transfer varchar(1) default 'n' not null
   ) ;

                                                                             