import com.cx.ng.aml0001a.service.impl.MetaDataService;
import com.cx.ng.aml0001a.core.ApplicationResourcesManager;
import com.cx.ng.aml0001a.core.RequestResourcesManager;
import com.cx.ng.common.NgResponse;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by cheng on 9/2/2016.
 */
public class SampleUnitTest {
    @Test
    public void testA() throws Exception {
        RequestResourcesManager requestResourcesManager = new RequestResourcesManager();
        requestResourcesManager.setApplicationResourcesManager(new ApplicationResourcesManager());
        MetaDataService service = new MetaDataService();
        service.setRequestResourcesManager(requestResourcesManager);
        try {
            NgResponse<String> result = service.iWillThrowException();
            Assert.fail("should throw exception");
        }catch (Exception e){
              //passed
        }

    }
}
