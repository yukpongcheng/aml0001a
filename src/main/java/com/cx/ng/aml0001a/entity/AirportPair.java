package com.cx.ng.aml0001a.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by cheng on 9/2/2016.
 */
public class AirportPair implements Serializable{

    private AirportPairKey airportPairKey;
    private int marketingMileage;
    private String typeCode;
    private String inactiveIndicator;
    private Date modifiedDate;

    public AirportPairKey getAirportPairKey() {
        return airportPairKey;
    }

    public void setAirportPairKey(AirportPairKey airportPairKey) {
        this.airportPairKey = airportPairKey;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getInactiveIndicator() {
        return inactiveIndicator;
    }

    public void setInactiveIndicator(String inactiveIndicator) {
        this.inactiveIndicator = inactiveIndicator;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public int getMarketingMileage() {
        return marketingMileage;
    }

    public void setMarketingMileage(int marketingMileage) {
        this.marketingMileage = marketingMileage;
    }

}
