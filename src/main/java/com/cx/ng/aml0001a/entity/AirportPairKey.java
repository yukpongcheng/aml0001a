package com.cx.ng.aml0001a.entity;

import java.io.Serializable;

/**
 * Created by cheng on 9/2/2016.
 */
public class AirportPairKey implements Serializable {
    private String oAirportCode;
    private String dAirportCode;
    private String carrierCode;

    public String getoAirportCode() {
        return oAirportCode;
    }

    public void setoAirportCode(String oAirportCode) {
        this.oAirportCode = oAirportCode;
    }

    public String getdAirportCode() {
        return dAirportCode;
    }

    public void setdAirportCode(String dAirportCode) {
        this.dAirportCode = dAirportCode;
    }

    public String getCarrierCode() {
        return carrierCode;
    }

    public void setCarrierCode(String carrierCode) {
        this.carrierCode = carrierCode;
    }
}
