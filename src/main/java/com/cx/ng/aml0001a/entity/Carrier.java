package com.cx.ng.aml0001a.entity;

import java.io.Serializable;

/**
 * Created by cheng on 19/8/2016.
 */
public class Carrier implements Serializable{
    private String code;
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
