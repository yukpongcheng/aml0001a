package com.cx.ng.aml0001a.service.impl;

import com.cx.ng.aml0001a.entity.Carrier;
import com.cx.ng.aml0001a.service.MetaDataServiceInterface;
import com.cx.ng.aml0001a.core.RequestResourcesManager;
import com.cx.ng.common.*;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cheng on 9/1/2016.
 */
@RequestScoped
@Path("/MetaDataService")
public class MetaDataService implements MetaDataServiceInterface {
    @Inject
    RequestResourcesManager requestResourcesManager;

    public void setRequestResourcesManager(RequestResourcesManager requestResourcesManager) {
        this.requestResourcesManager = requestResourcesManager;
    }

    @POST
    @Path("/getCarrierList/{fromAirportCode}/{toAirportCode}")
    @Produces(MediaType.APPLICATION_JSON)
    public NgResponse<List<Carrier>> getCarrierList3
            (@PathParam("fromAirportCode") String fromAirportCode, @PathParam("toAirportCode") String toAirportCode) throws Exception {
        NgResponse<List<Carrier>> response = new NgResponse<>();
        ResultSet rs = null;
        List<Carrier> result = new ArrayList<>();
        Connection conn = requestResourcesManager.getConnection();
        String sql = "select c.carrier_code, c.carrier_name from accrual_airport_pair map join carrier c on map.fkx1_carriercode = c.carrier_code\n" +
                "where fkx_o_airport_code = ? and fkx_d_airport_code=?";

        PreparedStatement ps_query = conn.prepareStatement(sql);
        ps_query.setString(1, fromAirportCode);
        ps_query.setString(2, toAirportCode);
        rs = ps_query.executeQuery();

        while (rs.next()) {
            Carrier c = new Carrier();
            c.setCode(rs.getString("carrier_code"));
            c.setName(rs.getString("carrier_name"));
            result.add(c);
        }
        ps_query.close();
        rs.close();
        response.setData(result);
        return response;
    }

//    @POST
//    @Path("/getCarrierList/{fromAirportCode}/{toAirportCode}")
//    @Produces(MediaType.APPLICATION_JSON)
//    public NgResponse<List<Carrier>> getCarrierList(@PathParam("fromAirportCode") String fromAirportCode, @PathParam("toAirportCode") String toAirportCode) throws Exception {
//        NgResponse<List<Carrier>> response = new NgResponse<>();
//        List<Carrier> result = new ArrayList<>();
//        Session session = requestResourcesManager.getHibernateSession();
//        String sql = "select c.carrier_code as code, c.carrier_name as name from accrual_airport_pair map join carrier c on map.fkx1_carriercode = c.carrier_code\n" +
//                " where fkx_o_airport_code = :fromAirportCode and fkx_d_airport_code= :toAirportCode";
//        Query q = session.createSQLQuery(sql).setResultTransformer( Transformers.aliasToBean( Carrier.class ) );
//        q.setParameter("fromAirportCode", fromAirportCode);
//        q.setParameter("toAirportCode", toAirportCode);
//        result = q.list();
//        response.setData(result);
//        return response;
//    }

//    @Override
//    @POST
//    @Path("/getCarrierList/{fromAirportCode}/{toAirportCode}")
//    @Produces(MediaType.APPLICATION_JSON)
//    public NgResponse<List<Carrier>> getCarrierList(@PathParam("fromAirportCode") String fromAirportCode, @PathParam("toAirportCode") String toAirportCode) throws Exception {
//        String sql = "select c.carrier_code as code, c.carrier_name as name from accrual_airport_pair map join carrier c on map.fkx1_carriercode = c.carrier_code\n" +
//                " where fkx_o_airport_code = :fromAirportCode and fkx_d_airport_code= :toAirportCode";
//        List<Carrier> result = requestResourcesManager.getSessionX().createScalarQuery(sql).
//                setParam("fromAirportCode", fromAirportCode).setParam("toAirportCode", toAirportCode).list(Carrier.class);
//        return new NgResponse<List<Carrier>>().setData(result);
//    }

    @GET
    @Path("/testException")
    @Produces(MediaType.APPLICATION_JSON)
    public NgResponse<String> iWillThrowException() {
        throw new NullPointerException("test exception");
    }

//    @Override
//    @POST
//    @Path("/getToAirportList/{fromAirportCode}")
//    @Produces(MediaType.APPLICATION_JSON)
//    public NgResponse<List<String>> getToAirportList2(@PathParam("fromAirportCode") String fromAirportCode) throws Exception {
//        String sql = "select distinct fkx_d_airport_code " +
//                "from accrual_airport_pair  " +
//                "where fkx_o_airport_code = :fromAirportCode ";
//        Session session = requestResourcesManager.getHibernateSession();
//        //one liner demo
//        return new NgResponse<List<String>>().setData(
//                session.
//                        createNativeQuery(sql).setParameter("fromAirportCode", fromAirportCode).list());
//    }

    @POST
    @Path("/getCarrierList/{fromAirportCode}/{toAirportCode}")
    @Produces(MediaType.APPLICATION_JSON)
    public NgResponse<List<Carrier>> getCarrierList(@PathParam("fromAirportCode") String fromAirportCode, @PathParam("toAirportCode") String toAirportCode) {

        try {
            ResultSet rs = null;
            Connection conn = requestResourcesManager.getConnection();
            String sql = "";
            List<Carrier> result = new ArrayList<>();
            sql = "select c.carrier_code, c.carrier_name from accrual_airport_pair map join carrier c on map.fkx1_carriercode = c.carrier_code\n" +
                    "where fkx_o_airport_code = ? and fkx_d_airport_code=?";

            PreparedStatement ps_query = conn.prepareStatement(sql);
            ps_query.setString(1, fromAirportCode);
            ps_query.setString(2, toAirportCode);
            rs = ps_query.executeQuery();

            while (rs.next()) {
                Carrier c = new Carrier();
                c.setCode(rs.getString("carrier_code"));
                c.setName(rs.getString("carrier_name"));
                result.add(c);
            }


            ps_query.close();
            rs.close();
            return new NgResponse<List<Carrier>>().setData(result);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }


    }

    @POST
    @Path("/getToAirportList/{fromAirportCode}")
    @Produces(MediaType.APPLICATION_JSON)
    public NgResponse<List<String>> getToAirportList(@PathParam("fromAirportCode") String fromAirportCode) {
        try {
            ResultSet rs;
            Connection conn = requestResourcesManager.getConnection();
            String sql = "";
            List<String> result = new ArrayList<>();

            sql = "select distinct fkx_d_airport_code " +
                    "from accrual_airport_pair  " +
                    "where fkx_o_airport_code = ? ";

            PreparedStatement ps_query = conn.prepareStatement(sql);
            ps_query.setString(1, fromAirportCode);
            rs = ps_query.executeQuery();

            while (rs.next()) {
                result.add(rs.getString("fkx_d_airport_code"));
            }


            ps_query.close();
            rs.close();
            return new NgResponse<List<String>>().setData(result);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

}
