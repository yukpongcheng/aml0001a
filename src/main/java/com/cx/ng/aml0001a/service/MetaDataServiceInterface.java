package com.cx.ng.aml0001a.service;

import com.cx.ng.aml0001a.entity.Carrier;
import com.cx.ng.common.NgResponse;


import java.util.List;

/**
 * Created by cheng on 9/2/2016.
 */
public interface MetaDataServiceInterface {

    NgResponse<List<Carrier>> getCarrierList( String fromAirportCode,String toAirportCode) throws Exception;

    NgResponse<List<String>> getToAirportList( String fromAirportCode) throws Exception;
}
