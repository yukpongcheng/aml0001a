package com.cx.ng.aml0001a.core;

/**
 * Created by cheng on 17/8/2016.
 */
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/rpc")
public class RestApp extends Application
{ }
