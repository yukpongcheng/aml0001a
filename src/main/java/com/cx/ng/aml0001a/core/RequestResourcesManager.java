package com.cx.ng.aml0001a.core;


import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;

/**
 * Created by cheng on 9/1/2016.
 */
@RequestScoped
public class RequestResourcesManager {
    @Inject
    ApplicationResourcesManager applicationResourcesManager;

    private Connection connection;
    public void setApplicationResourcesManager(ApplicationResourcesManager applicationResourcesManager) {
        this.applicationResourcesManager = applicationResourcesManager;
    }

//    private Session hibernateSession;
//    public Session getHibernateSession(){
//        if (hibernateSession == null) {
//            hibernateSession = applicationResourcesManager.getSessionFactory().openSession();
//        }
//        return hibernateSession;
//    }

    public Connection getConnection()
    {
        if(connection == null){
            try {
                DataSource ds = (DataSource) applicationResourcesManager.getConnectionContext().lookup("jdbc/aml0001a_datasource");
                connection = ds.getConnection();
            } catch (Exception e) {
                System.out.println(e.getMessage());
                throw new RuntimeException(e);
            }
        }
        return connection;
//        return ((SessionImpl)getHibernateSession()).connection();
    }

    @PreDestroy
    public void preDestroy(){
        try{
            if(connection != null && !connection.isClosed()){
                connection.close();
            }
//            if(hibernateSession != null && hibernateSession.isOpen())
//                hibernateSession.close();
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
