package com.cx.ng.aml0001a.core;

import javax.inject.Inject;
import javax.naming.NamingException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by cheng on 9/1/2016.
 */
public class ApplicationInitiator implements ServletContextListener {
    @Inject
    private ApplicationResourcesManager bean;
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }

    public void contextInitialized(ServletContextEvent servletContextEvent) {
        try {
            bean.init();
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }
}
