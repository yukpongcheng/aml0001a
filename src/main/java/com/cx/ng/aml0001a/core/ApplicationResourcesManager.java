package com.cx.ng.aml0001a.core;

//import org.hibernate.SessionFactory;
//import org.hibernate.cfg.Configuration;

import javax.enterprise.context.ApplicationScoped;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Created by cheng on 9/1/2016.
 */
@ApplicationScoped
public class ApplicationResourcesManager {

    //    private SessionFactory sessionFactory;
    private Context connContext;
    public void init() throws NamingException {
        System.out.println("init ApplicationResourcesManager");
        Context initContext = new InitialContext();
        connContext  = (Context)initContext.lookup("java:/comp/env");
//        sessionFactory = new Configuration().configure("hibernate.cnf.xml").buildSessionFactory();

    }

//    public SessionFactory getSessionFactory() {
//        return sessionFactory;
//    }

    public Context getConnectionContext(){
        return connContext;
    }
}
